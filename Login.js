import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image
} from 'react-native';
import * as React from 'react';
import arrowBack from './assets/arrow-back.png';


export default function Login({navigation}) {
  const [nom, setNom] = React.useState('');
  const [prenom, setPrenom] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [secondPassword, setSecondPassword] = React.useState('');
  const [age, setAge] = React.useState('');
  const [email, setEmail] = React.useState('');

  const checkPassword = (password, secondPassword) => {
    if (password === secondPassword) {
      return true;
    }
    return false;
  };

  const sendSubscription = (
    nom,
    prenom,
    age,
    email,
    password,
    secondPassword,
  ) => {
    if (checkPassword(password, secondPassword)) {
      const subscription = {
        nom: nom,
        prenom: prenom,
        age: age,
        email: email,
      };
      console.log(subscription);
    } else {
      alert('Les mots de passe ne correspondent pas');
    }
  };

  return (
    <View style={styles.container}>
    <TouchableOpacity style={styles.back}>
    <Image source={arrowBack} style={styles.arrowBack} />
    </TouchableOpacity>
      <View>
        <Text style={styles.titre}>INSCRIPTION</Text>
        <View style={styles.inputContainer}>
          <TextInput
            placeholderTextColor="#000"
            style={styles.input}
            placeholder="Nom"
            onChangeText={text => setNom(text)}
          />
          <TextInput
            placeholderTextColor="#000"
            style={styles.input}
            placeholder="Prénom"
            onChangeText={text => setPrenom(text)}
          />
          <TextInput
            placeholderTextColor="#000"
            style={styles.input}
            keyboardType="numeric"
            placeholder="Âge"
            onChangeText={text => setAge(text)}
          />
          <TextInput
            placeholderTextColor="#000"
            style={styles.input}
            placeholder="Email"
            onChangeText={text => setEmail(text)}
          />
          <TextInput
            placeholderTextColor="#000"
            style={styles.input}
            placeholder="Mot de passe"
            secureTextEntry={true}
            onChangeText={text => setPassword(text)}
          />
          <TextInput
            placeholderTextColor="#000"
            style={styles.input}
            placeholder="Confirmation"
            secureTextEntry={true}
            onChangeText={text => setSecondPassword(text)}
          />
          <TouchableOpacity
            style={styles.button}
            onPress={() =>
              sendSubscription(
                nom,
                age,
                prenom,
                email,
                password,
                secondPassword,
              )
            }>
            <Text style={styles.textButton}>S'inscrire</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.buttonTwo}
            onPress={() => navigation.navigate('Log')}>
            <Text style={styles.textButton2}>Déjà inscrit ?</Text>
          </TouchableOpacity>
        </View>
      </View>
      <Text style={styles.conditions}>
        En continuant, vous acceptez à la fois la{' '}
        <Text style={styles.underline}>politique de confidentialité</Text> et
        les <Text style={styles.underline}>conditions de services</Text>
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  colors: {
    blue: '#1E1143',
    yellow: '#E180B3',
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  titre: {
    fontSize: 32,
    color: '#1E1143',
    fontWeight: '800',
    letterSpacing: -0.3,
    textAlign: 'center',
    marginBottom: 30,
  },
  input: {
    backgroundColor: 'white',
    borderRadius: 6,
    borderWidth: 2,
    borderColor: '#E180B3',
    height: 40,
    width: 320,
    marginBottom: 15,
    paddingHorizontal: 10,
  },
  button: {
    marginTop: 15,
    marginBottom: 15,
    backgroundColor: '#1E1143',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    width: 320,
    borderRadius: 6,
  },
  buttonTwo: {
    marginBottom: 15,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    width: 320,
    borderRadius: 6,
  },
  textButton: {
    color: '#fff',
    fontSize: 18,
    fontWeight: '700',
  },
  conditions: {
    color: '#1E1143',
    fontSize: 11,
    marginTop: 15,
    width: 225,
    textAlign: 'center',
  },
  underline: {
    textDecorationLine: 'underline',
  },
  textButton2: {
    color: '#1E1143',
    fontSize: 18,
    fontWeight: '700',
  },
  back:{
    position: 'absolute',
    top: 80,
    left: 40,
  } 
});
