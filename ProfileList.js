import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  SafeAreaView,
  ImageBackground,
  Button,
} from 'react-native';
import * as React from 'react';
import profile from './assets/profile.png';
import search from './assets/search.png';
import lea from './assets/lea.png';
import paul from './assets/paul.png';
import will from './assets/will.png';
import frank from './assets/frank.png';
import lola from './assets/lola.png';
import jade from './assets/jade.png';
import jules from './assets/jules.png';
import antoine from './assets/antoine.png';
import remi from './assets/remi.png';
import jenn from './assets/jenn.png';
import julie from './assets/julie.png';
import meryl from './assets/meryl.png';
import doubledown from './assets/doubledown.png';

import Menu from './Menu';

export default function ProfileList({navigation}) {
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <ScrollView
          style={styles.test}
          contentContainerStyle={{paddingBottom: 300}}>
          <View style={styles.header}>
            <View style={styles.logo}>
              <Text>STULEAD</Text>
            </View>
            <View style={styles.headerButton}>
              <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
                <Image source={profile} style={styles.profile} />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate('Search')}>
                <Image source={search} style={styles.search} />
              </TouchableOpacity>
            </View>
          </View>

          <TouchableOpacity style={styles.filter}>
          <Image source={doubledown} />
            <Text style={styles.filterText}>Filtre</Text>
          </TouchableOpacity>

          <View style={styles.sectionRow}>
            <TouchableOpacity style={styles.imageBottom} onPress={() => navigation.navigate('Person', { name : 'Paul Barker', activity : 'Expert Artistique', image: paul}) }>
              <Image source={paul} style={styles.image} />
              <Text style={styles.profileName}>Paul Barker</Text>
              <Text style={styles.profileActivity}>Expert Artistique</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.imageBottom} onPress={() => navigation.navigate('Person', { name : 'Jade Lepladec', activity : 'Expert Marketing', image: jade}) }>
              <Image source={jade} style={styles.image} />
              <Text style={styles.profileName}>Jade Lepladec</Text>
              <Text style={styles.profileActivity}>Expert Marketing</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.imageBottom} onPress={() => navigation.navigate('Person', { name : 'Remi Ruby', activity : 'Expert developper', image: remi}) }>
              <Image source={remi} style={styles.image} />
              <Text style={styles.profileName}>Remi Ruby</Text>
              <Text style={styles.profileActivity}>Expert Developper</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.imageBottom} onPress={() => navigation.navigate('Person', { name : 'Merylle Yoka', activity : 'Expert Mode', image: meryl}) }>
              <Image source={meryl} style={styles.image} />
              <Text style={styles.profileName}>Merylle Yoka</Text>
              <Text style={styles.profileActivity}>Expert Mode</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.imageBottom} onPress={() => navigation.navigate('Person', { name : 'Lola Smith', activity : 'Expert', image: lola}) }>
              <Image source={lola} style={styles.image} />
              <Text style={styles.profileName}>Lola Smith</Text>
              <Text style={styles.profileActivity}>Expert</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.imageBottom} onPress={() => navigation.navigate('Person', { name : 'Julie Lepond', activity : 'Expert Droit', image: julie}) }>
              <Image source={julie} style={styles.image} />
              <Text style={styles.profileName}>Julie Lepond</Text>
              <Text style={styles.profileActivity}>Expert Droit</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.imageBottom} onPress={() => navigation.navigate('Person', { name : 'Jules Perez', activity : 'Expert du numérique', image: jules}) }>
              <Image source={jules} style={styles.image} />
              <Text style={styles.profileName}>Jules Perez</Text>
              <Text style={styles.profileActivity}>Expert du numérique</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.imageBottom} onPress={() => navigation.navigate('Person', { name : 'Frank Bombart', activity : 'Expert Administration', image: frank}) }>
              <Image source={frank} style={styles.image} />
              <Text style={styles.profileName}>Frank Bombart</Text>
              <Text style={styles.profileActivity}>Expert Administration</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.imageBottom} onPress={() => navigation.navigate('Person', { name : 'Antoine Beney', activity : 'Expert Vente', image: antoine}) }>
              <Image source={antoine} style={styles.image} />
              <Text style={styles.profileName}>Antoine Beney</Text>
              <Text style={styles.profileActivity}>Expert Vente</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.imageBottom} onPress={() => navigation.navigate('Person', { name : 'Lea Rossi', activity : 'Expert Droit', image: lea}) }>
              <Image source={lea} style={styles.image} />
              <Text style={styles.profileName}>Lea Rossi</Text>
              <Text style={styles.profileActivity}>Expert Droit</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <Menu />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  colors: {},
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignITems: 'center',
    width: '100%',
    height: 60,
    paddingHorizontal: 20,
  },
  headerButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '15%',
  },
  sectionRow: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  test: {
    width: '100%',
    height: 1000,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  imageBottom: {
    marginTop: 20,
  },
  filter: {
    borderWidth: 3,
    width: 100,
    height: 25,
    borderColor: '#F37EC1',
    borderRadius: 7,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
    flexDirection: 'row',
  },
  filterText: {
    color: '#F37EC1',
    fontSize: 15,
    fontWeight: 'bold',
    marginLeft:5,
  },
});
