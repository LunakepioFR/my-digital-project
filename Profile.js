import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  SafeAreaView,
  ImageBackground,
  Button,
} from 'react-native';
import * as React from 'react';
import profile from './assets/profile.png';
import search from './assets/search.png';
import arrowRight from './assets/arrowRight.png';
import profileHighlight from './assets/profileHighlight.png';

import Menu from './Menu';

export default function Profile({navigation}) {
    const [dontDisturb, setDontDisturb] = React.useState(false);
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <ScrollView
          style={styles.test}
          contentContainerStyle={{paddingBottom: 300}}>
          <View style={styles.header}>
            <View style={styles.logo}>
              <Text>STULEAD</Text>
            </View>
            <View style={styles.headerButton}>
              <TouchableOpacity>
                <Image source={profileHighlight} style={styles.profile} />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate('Search')}>
                <Image source={search} style={styles.search} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.menu}>
            <View style={styles.cyan}>
              <Text style={styles.textWhite}>Profil</Text>
            </View>

            <TouchableOpacity style={styles.menuButton}>
              <View style={styles.menuLeft}>
                <Image source={profile} style={styles.menuImage} />
                <Text style={styles.buttonText}>Profil</Text>
              </View>
              <View style={styles.menuRight}>
                <Image source={arrowRight} style={styles.menuArrow} />
              </View>
            </TouchableOpacity>

            <View style={styles.cyan}>
              <Text style={styles.textWhite}>DEVICE</Text>
            </View>

            <TouchableOpacity style={styles.menuButton} onPress={() => setDontDisturb(!dontDisturb)}>
              <View style={styles.menuLeft}>
                <Image source={profile} style={styles.menuImage} />
                <Text style={styles.buttonText}>Ne pas déranger</Text>
              </View>
              <View style={styles.menuRight}>
              <View style={dontDisturb ? styles.toggled : styles.toggle}><View style={styles.circle}/></View>
              </View>
            </TouchableOpacity>

            <TouchableOpacity style={styles.menuButton}>
              <View style={styles.menuLeft}>
                <Image source={profile} style={styles.menuImage} />
                <Text style={styles.buttonText}>Notifications</Text>
              </View>
              <View style={styles.menuRight}>
              <Text style={styles.menuRight}>Lorsqu'il est déverouiller</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity style={styles.menuButton}>
              <View style={styles.menuLeft}>
                <Image source={profile} style={styles.menuImage} />
                <Text style={styles.buttonText}>Politique</Text>
              </View>
              <View style={styles.menuRight}>
                <Text style={styles.menuRight}>Confidentialité</Text>
              </View>
            </TouchableOpacity>

          </View>
        </ScrollView>
        <Menu />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  colors: {},
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignITems: 'center',
    width: '100%',
    height: 60,
    paddingHorizontal: 20,
  },
  headerButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '15%',
  },
  background: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: '100%',
    height: 300,
    marginTop: -20,
  },
  box: {
    width: '100%',
    height: '70%',
    justifyContent: 'space-between',
    padding: 20,
  },
  test: {
    width: '100%',
    height: 1000,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  cyan :{
    backgroundColor: '#31B3BF',
    padding: 10,
    height: 60,
    flexDirection: 'row',
    alignItems: 'center',
  },
  textWhite: {
    color: '#fff',
    fontSize: 18,
    fontWeight: 'bold',
  },
  menuButton:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height:55,
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#F37EC1',
  },
  menuLeft: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonText:{
    fontSize: 16,
    fontWeight: 'bold',
    marginLeft: 10,
  },
  menuRight: {
    fontWeight: 'bold',
    fontSize: 13,
  },
  toggle:{
    width: 35,
    height: 20,
    borderRadius: 15,
    padding: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    backgroundColor: '#000',

  },
  toggled:{
    width: 35,
    height: 20,
    backgroundColor: '#F37EC1',

    borderRadius: 15,
    padding: 1,
    alignItems: 'center',
    flexDirection: 'row',
  },
  circle:{
    width: 18,
    height: 18,
    borderRadius: 30,
    backgroundColor: '#fff',
  },
});
