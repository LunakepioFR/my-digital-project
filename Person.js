import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  SafeAreaView,
  ImageBackground,
  Button,
} from 'react-native';
import * as React from 'react';
import arrowBack from './assets/arrow-back.png';
import arrowRight from './assets/arrowRight.png';
import missions from './assets/missions.png';
import profil from './assets/profil.png';
import formations from './assets/formations.png';
import insertion from './assets/inserition.png';
import remuneration from './assets/remuneration.png';
import evolution from './assets/evolution.png';
import present from './assets/else.png';
import Menu from './Menu';

export default function Person({navigation, route}) {
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <ScrollView style={styles.test} contentContainerStyle={{paddingBottom: 300}}>
          <View style={styles.header}>
            <TouchableOpacity
              onPress={() => navigation.navigate('ProfileList')}>
              <Image source={arrowBack} style={styles.arrowBack} />
            </TouchableOpacity>
            <View style={styles.headerLeft}>
              <Text style={styles.name}>{route.params.name}</Text>
              <Text style={styles.activity}>{route.params.activity}</Text>
            </View>
            <View style={styles.headerRight}>
              <TouchableOpacity style={styles.subscribe}>
                <Text style={styles.plus}>+</Text>
                <Text style={styles.sub}>S'abonner</Text>
              </TouchableOpacity>
            </View>
          </View>
          <Image source={route.params.image} style={styles.image} />

          <View style={styles.description}>
            <Text style={styles.descriptionText}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Pellentesque euismod, nisi vel consectetur consectetur, nisi nisl
              aliquet nisl, eget consectetur nisl nisl euismod nisi.
            </Text>
          </View>

          <View style={styles.sectionList}>
            <TouchableOpacity style={styles.listItem}>
              <View style={styles.listItemLeft}>
                <Image source={missions} />
                <Text style={styles.listItemText}>Missions</Text>
              </View>
              <View style={styles.listItemRight}>
                <Image source={arrowRight} />
              </View>
            </TouchableOpacity>

            <TouchableOpacity style={styles.listItem}>
              <View style={styles.listItemLeft}>
                <Image source={profil} />
                <Text style={styles.listItemText}>Profil</Text>
              </View>
              <View style={styles.listItemRight}>
                <Image source={arrowRight} />
              </View>
            </TouchableOpacity>

            <TouchableOpacity style={styles.listItem}>
              <View style={styles.listItemLeft}>
                <Image source={formations} />
                <Text style={styles.listItemText}>Formation</Text>
              </View>
              <View style={styles.listItemRight}>
                <Image source={arrowRight} />
              </View>
            </TouchableOpacity>

            <TouchableOpacity style={styles.listItem}>
              <View style={styles.listItemLeft}>
                <Image source={insertion} />
                <Text style={styles.listItemText}>Insertion professionnelle</Text>
              </View>
              <View style={styles.listItemRight}>
                <Image source={arrowRight} />
              </View>
            </TouchableOpacity>

            <TouchableOpacity style={styles.listItem}>
                <View style={styles.listItemLeft}>
                    <Image source={remuneration} />
                    <Text style={styles.listItemText}>Remuneration</Text>
                    </View>
                    <View style={styles.listItemRight}>
                        <Image source={arrowRight} />
                        </View>
            </TouchableOpacity>

            <TouchableOpacity style={styles.listItem}>
                <View style={styles.listItemLeft}>
                    <Image source={evolution} />
                    <Text style={styles.listItemText}>Evolution de carrière</Text>
                    </View>
                    <View style={styles.listItemRight}>
                        <Image source={arrowRight} />
                        </View>
            </TouchableOpacity>

            <TouchableOpacity style={styles.listItem}>
                <View style={styles.listItemLeft}>
                    <Image source={present} />
                    <Text style={styles.listItemText}>What else ?</Text>
                    </View>
                    <View style={styles.listItemRight}>
                        <Image source={arrowRight} />
                        </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <Menu />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  test: {
    width: '100%',
    height: 1000,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  header: {
    width: '100%',
    height: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 20,
  },
  headerLeft: {
    marginLeft: -60,
  },
  name: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#31B3BF',
  },
  subscribe: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    width: 110,
    height: 30,
    borderRadius: 100,
    borderWidth: 2,
    borderColor: '#31B3BF',
  },
  plus: {
    fontSize: 20,
    color: '#31B3BF',
    fontWeight: 'bold',
  },
  sub: {
    color: '#31B3BF',
  },
  image: {
    flex: 1,
    width: 400,
    height: 400,
    resizeMode: 'contain',
  },
  descriptionText: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  listItem : {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 20,
    borderTopWidth: 1,
    borderRightWidth:1,
    borderLeftWidth:1,
    borderColor: '#F37EC1',
  },
  listItemText: {
    fontSize: 15,
    fontWeight: 'bold',
    color: '#1E1143',
    marginLeft:15,
  },
  listItemLeft: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
