import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  Image,
} from 'react-native';
import * as React from 'react';
import intro from './assets/intro.png';

export default function Intro({navigation}) {

  setTimeout(() => {
    navigation.navigate('ChooseLogin');
  }
  , 4000);
  return (
    <ImageBackground source={intro} style={styles.image}>
    <View style={styles.container}>
    </View>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  colors: {
    blue: '#0F1C43',
    yellow: '#FBB709',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
   image:{
    flex: 1,
    resizeMode: 'cover',
   }
});
