import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Image,
  } from 'react-native';
  import * as React from 'react';
  import arrowBack from './assets/arrow-back.png';


  export default function Info1({navigation}) {
    const [sexe, setSexe] = React.useState(true);
    const [postalCode, setPostalCode] = React.useState('');
    const [newsletter, setNewsletter] = React.useState(false);
    const [rules, setRules] = React.useState(false);
    const [terms, setTerms] = React.useState(false);
    return (
      <View style={styles.container}>
          <TouchableOpacity style={styles.back} onPress={() => navigation.navigate('Login')}>
    <Image source={arrowBack} style={styles.arrowBack} />
    </TouchableOpacity>
        <View style={styles.header}>
            <View style={styles.dotFull} />
            <View style={styles.dot} />
            <View style={styles.dot} />
        </View>
        <View style={styles.box}>
            <Text style={styles.title}>Faisons plus connaissance !</Text>
            <View style={styles.boxContainer}>
                <TouchableOpacity style={sexe ? styles.boxYellow : styles.boxWhite} onPress={() => setSexe(true)}>
                    <Text style={sexe ? styles.boxItemTitleYellow : styles.boxItemTitle}>♂</Text>
                    <Text style={sexe ? styles.boxItemTextYellow: styles.boxItemText}>Homme</Text>
                </TouchableOpacity>
                <TouchableOpacity style={sexe ? styles.boxWhite : styles.boxYellow} onPress={() => setSexe(false)}>
                    <Text style={sexe ? styles.boxItemTitle : styles.boxItemTitleYellow}>♀</Text>
                    <Text style={sexe ? styles.boxItemText : styles.boxItemTextYellow}>Femme</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.form}>
                <Text style={styles.formTitle}>
                    Votre code postal
                </Text>
                <TextInput placeholderTextColor="#333" style={styles.formInput} placeholder="Code postal" onChangeText={(text) => setPostalCode(text)}/>
                <TouchableOpacity style={styles.row} onPress={() => setNewsletter(!newsletter)}>
                    <View style={newsletter ? styles.formButtonTicked : styles.formButton}/><View><Text style={styles.formText}>Je m'inscris à la newsletter pour être tenu-e au courant des nouveautés.</Text></View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.row} onPress={() => setTerms(!terms)}>
                    <View style={terms ? styles.formButtonTicked : styles.formButton}/><View><Text style={styles.formText}>En m'inscrivant, j'accepte de me conformer {'\n'}
                    à la politique de confidentialité et aux conditions générale.</Text></View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.row} onPress={() => setRules(!rules)}>
                    <View style={rules ? styles.formButtonTicked : styles.formButton}/><View><Text style={styles.formText}>Je ne souhaite pas donner ces informations.</Text></View>
                </TouchableOpacity>
            </View>
            <TouchableOpacity style={styles.buttonTwo} onPress={() => navigation.navigate('Info2') }>
            <Text style={styles.textButton}>Continuer</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.precision}><Text style={styles.lowText}>Vous pouvez à tout moment changer vos{'\n'} informations dans les paramètres</Text></View>
      </View>
    );
  }

  const styles = StyleSheet.create({
    colors: {
        blue: '#1E1143',
        yellow: '#EE7CBD',
    },
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    box: {
        height:'70%',
        width:'90%',
        borderWidth:2,
        borderColor:'#31B3BF',
        alignItems:'center',
        padding:20,
    },
    title:{
        fontSize:25,
        color:'#1E1143',
        fontWeight:'800',
        letterSpacing:-0.3,
        textAlign:'center',
    },
    header: {
        width:'100%',
        height:'15%',
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'row',
        position:'absolute',
        top:30,
    },
    dot: {
        width:15,
        height:15,
        borderRadius:50,
        borderWidth:1,
        borderColor:'#31B3BF',
        margin:5,
    },
    dotFull: {
        width:15,
        height:15,
        borderRadius:50,
        backgroundColor:'#EE7CBD',
        borderWidth:1,
        borderColor:'#31B3BF',
        margin:5,
    },
    formTitle:{
        fontSize:15,
        color:'#1E1143',
    },
    boxContainer: {
        marginTop:30,
        width:'80%',
        height:100,
        flexDirection:'row',
        justifyContent:'space-around',
        alignItems:'center',
    },
    boxItem: {
        width:100,
        height:100,
        backgroundColor:'#fff',
        justifyContent:'center',
        alignItems:'center',
    },
    boxWhite: {
        width:100,
        height:100,
        backgroundColor:'#fff',
        justifyContent:'center',
        alignItems:'center',
        borderWidth:3,
        borderColor:'#1E1143',
    },
    boxYellow: {
        width:100,
        height:100,
        backgroundColor:'#F37EC1',
        justifyContent:'center',
        alignItems:'center',
    },
    boxItemTitle:{
        fontSize:50,
        color:'black',
        fontWeight:'900',
    },
    boxItemTitleYellow:{
        fontSize:50,
        color:'#1E1143',
        fontWeight:'900',
    },
    boxItemText:{
        color: '#1E1143',
    },
    boxItemTextYellow:{
        color: '#1E1143',
    },
    form:{
        width:'90%',
        textAlign:'left',
        marginTop:30,
    },
    formInput:{
        marginTop:15,
        backgroundColor: 'white',
        borderRadius: 6,
        borderWidth: 1,
        borderColor: '#EE7CBD',
        height: 30,
        width: "100%",
        marginBottom: 15,
        paddingHorizontal: 10,
    },
    row:{
        flexDirection:'row',
        alignItems:'center',
        height:60,
    },
    buttonTwo:{
        marginTop:30,
        backgroundColor: '#EE7CBD',
        justifyContent: 'center',
        alignItems: 'center',
        height: 30,
        width: 225,
        borderRadius: 6,
    },
    formButton:{
        width: 20,
        height: 20,
        backgroundColor:'#fff',
        borderWidth:2,
        borderColor:'#EE7CBD',
    },
    textButton: {
        color: '#0F1C43',
        fontSize: 14,
        fontWeight: '700',
      },
    formButtonTicked:{
        width: 20,
        height: 20,
        backgroundColor:'#F37EC1',
        borderWidth:2,
        borderColor:'#EE7CBD',
    },
    formText:{
        fontSize:9,
        color:'#1E1143',
        textAlign:'left',
        marginLeft:7,
    },
    lowText: {
        fontSize: 9,
        color: '#1E1143',
        textAlign: 'center',
      },
      precision: {
        textAlign: 'center',
        position: 'absolute',
        bottom: 30,
      },
      back:{
        position: 'absolute',
        top: 80,
        left: 40,
        zIndex:2,
    },
  });

