import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  SafeAreaView,
  ImageBackground,
  Button,
} from 'react-native';
import * as React from 'react';
import profile from './assets/profile.png';
import search from './assets/search.png';
import image1 from './assets/image-Home1.png';
import quizz from './assets/quizz-icon.png';
import mds from './assets/mds.png';
import mmi from './assets/mmi.png';
import iesa from './assets/iesa.png';
import tc from './assets/tc.png';
import decouvert from './assets/decouvert.png';
import salonetudiant from './assets/salonetudiant.png';
import jpg from './assets/jpg.png';
import innitiation from './assets/innitiation.png';
import contentcreator from './assets/contentcreator.png';
import cashier from './assets/cashier.png';
import graphicdesigner from './assets/graphicdesigner.png';
import marketing from './assets/marketing.png';

import Menu from './Menu';

export default function Home({navigation}) {
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <ScrollView
          style={styles.test}
          contentContainerStyle={{paddingBottom: 300}}>
          <View style={styles.header}>
            <View style={styles.logo}>
              <Text>STULEAD</Text>
            </View>
            <View style={styles.headerButton}>
              <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
                <Image source={profile} style={styles.profile} />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate('Search')}>
                <Image source={search} style={styles.search} />
              </TouchableOpacity>
            </View>
          </View>

          <ImageBackground source={image1} style={styles.background}>
            <View style={styles.box}>
              <Text style={styles.textBox}>
                Bienvenue sur Stulead ! {'\n'} {'\n'}Une application qui te
                permet de trouver ta voie. Grace aux vidéos de présentation de
                métiers, au quiz, au professionnel... Tu pourras mieux
                t'orienter. N'hésite pas à faire appel à un expert pour t'aider
                sur tes candidatures.{' '}
              </Text>
              <TouchableOpacity style={styles.seeMoreButton} onPress={() => navigation.navigate('ProfileList')}>
                <Text style={styles.buttonText}>Voir les profils</Text>
              </TouchableOpacity>
            </View>
          </ImageBackground>

          <View style={styles.section}>
            <Text style={styles.sectionTitle}>Test d'orientation</Text>
            <Image source={quizz} style={styles.quizz} />
          </View>

          <View style={styles.section}>
            <Text style={styles.sectionTitle}>Les écoles</Text>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              <Image source={mds} style={styles.scrollableImage} />
              <Image source={mmi} style={styles.scrollableImage} />
              <Image source={iesa} style={styles.scrollableImage} />
              <Image source={tc} style={styles.scrollableImage} />
            </ScrollView>
          </View>

          <View style={styles.section}>
            <Text style={styles.sectionTitle}>Les événements</Text>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              <Image source={decouvert} style={styles.scrollableImage} />
              <Image source={salonetudiant} style={styles.scrollableImage} />
              <Image source={jpg} style={styles.scrollableImage} />
              <Image source={innitiation} style={styles.scrollableImage} />
            </ScrollView>
          </View>

          <View style={styles.section}>
            <Text style={styles.sectionTitle}>Suggestion</Text>
            <View style={styles.sectionRow}>
              <TouchableOpacity>
                <Image source={contentcreator} />
              </TouchableOpacity>
              <TouchableOpacity>
                <Image source={cashier} />
              </TouchableOpacity>
              <TouchableOpacity style={styles.bottomImage}>
                <Image source={graphicdesigner} />
              </TouchableOpacity>
              <TouchableOpacity style={styles.bottomImage}>
                <Image source={marketing} />
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        <Menu />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  colors: {},
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignITems: 'center',
    width: '100%',
    height: 60,
    paddingHorizontal: 20,
  },
  headerButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '15%',
  },
  background: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: '100%',
    height: 300,
    marginTop: -20,
  },
  box: {
    width: '100%',
    height: '70%',
    justifyContent: 'space-between',
    padding: 20,
  },
  seeMoreButton: {
    width: 150,
    height: 40,
    backgroundColor: '#1E1143',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },
  buttonText: {
    color: 'white',
  },
  test: {
    width: '100%',
    height: 1000,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  textBox: {
    color: '#1E1143',
    fontWeight: '600',
  },
  section: {
    width: '100%',
    justifyContent: 'center',
  },
  sectionTitle: {
    marginTop: 30,
    fontSize: 20,
    color: '#1E1143',
    fontWeight: '800',
    paddingLeft: 10,
    marginBottom: 15,
  },
  quizz: {
    alignSelf: 'center',
  },
  scrollableImage: {
    marginRight: 25,
    marginLeft: 10,
  },
  sectionRow: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  bottomImage: {
    marginTop: 20,
  },
});
