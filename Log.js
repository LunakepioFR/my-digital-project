import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image
} from 'react-native';
import * as React from 'react';
import arrowBack from './assets/arrow-back.png';

export default function Log({navigation}) {
  const [password, setPassword] = React.useState('');
  const [email, setEmail] = React.useState('');

  function onPressHandler() {
    if(password === '123' && email === 'Toto'){
      navigation.navigate('Home');
    } else {
      alert('Email ou mot de passe incorrect, utilisez : "toto" et "123"');
    }
  }

  return (
    <View style={styles.container}>
        <TouchableOpacity style={styles.back} onPress={() => navigation.navigate('Login')}>
    <Image source={arrowBack} style={styles.arrowBack} />
    </TouchableOpacity>
      <View>
        <Text style={styles.titre}>CONNEXION</Text>
        <View style={styles.inputContainer}>
          <TextInput
            placeholderTextColor="#000"
            style={styles.input}
            placeholder="Email"
            onChangeText={text => setEmail(text)}
          />
          <TextInput
            placeholderTextColor="#000"
            style={styles.input}
            placeholder="Mot de passe"
            secureTextEntry={true}
            onChangeText={text => setPassword(text)}
          />
          <TouchableOpacity style={styles.button} onPress={() => onPressHandler()}>
            <Text style={styles.textButton}>Se connecter</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  colors: {
    blue: '#1E1143',
    yellow: '#E180B3',
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  titre: {
    fontSize: 32,
    color: '#1E1143',
    fontWeight: '800',
    letterSpacing: -0.3,
    textAlign: 'center',
    marginBottom: 30,
  },
  input: {
    backgroundColor: 'white',
    borderRadius: 6,
    borderWidth: 2,
    borderColor: '#E180B3',
    height: 40,
    width: 320,
    marginBottom: 15,
    paddingHorizontal: 10,
  },
  button: {
    marginTop: 15,
    marginBottom: 15,
    backgroundColor: '#1E1143',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    width: 320,
    borderRadius: 6,
  },
  buttonTwo: {
    marginBottom: 15,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    width: 320,
    borderRadius: 6,
  },
  textButton: {
    color: '#fff',
    fontSize: 18,
    fontWeight: '700',
  },
  conditions: {
    color: '#fff',
    fontSize: 11,
    marginTop: 15,
    width: 225,
  },
  underline: {
    textDecorationLine: 'underline',
  },
  textButton2: {
    color: '#1E1143',
    fontSize: 18,
    fontWeight: '700',
  },
  back:{
    position: 'absolute',
    top: 80,
    left: 40,
  } ,
});
