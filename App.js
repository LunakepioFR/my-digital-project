/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
// eslint-disable-next-line no-unused-vars
import Realm from 'realm';
import Login from './Login';
import Log from './Log';
import Intro from './Intro';
import Info1 from './Info1';
import Info2 from './Info2';
import Info3 from './Info3';
import Home from './Home';
import Person from './Person';
import ProfileList from './ProfileList';
import ChooseLogin from './ChooseLogin';
import Profile from './Profile';
import Search from './Search';

const Stack = createNativeStackNavigator();

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}>

        <Stack.Screen name="Intro" component={Intro} />
        <Stack.Screen name="ChooseLogin" component={ChooseLogin} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Log" component={Log} />
        <Stack.Screen name="Info1" component={Info1} />
        <Stack.Screen name="Info2" component={Info2} />
        <Stack.Screen name="Info3" component={Info3} />
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Search" component={Search} />
        <Stack.Screen name="Profile" component={Profile} />
        <Stack.Screen name="ProfileList" component={ProfileList} />
        <Stack.Screen name="Person" component={Person} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({});

export default App;
