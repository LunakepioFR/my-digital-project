import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Image,
  } from 'react-native';
  import * as React from 'react';
  import { Dimensions } from 'react-native';
import notifIcon from './assets/notif-icon.png';
import chatIcon from './assets/chat-icon.png';
import callIcon from './assets/call-icon.png';
import addIcon from './assets/add-icon.png';
import spaceIcon from './assets/space-icon.png';
const windowHeight = Dimensions.get('window').height;
const testHeight = windowHeight - 130;
export default function Menu({navigation}) {
    return (
        <View style={styles.menu}>
            <TouchableOpacity onPress={() => navigation.navigate('Home')}>
            <Image source={addIcon} style={styles.addIcon} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Home')}>
            <Image source={chatIcon} style={styles.chatIcon} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Home')}>
            <Image source={callIcon} style={styles.callIcon} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Home')}>
            <Image source={notifIcon} style={styles.notifIcon} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Home')}>
            <Image source={spaceIcon} style={styles.spaceIcon} />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    menu: {
        position: 'absolute',
        top: testHeight,
        left: 0,
        right: 0,
        borderTopWidth: 2,
        borderColor : '#0F1C43',
        width: '100%',
        backgroundColor: 'white',
        height: 90,
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingHorizontal: 20,
        paddingTop:20,
    },
  });

