import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  SafeAreaView,
  ImageBackground,
  Button,
} from 'react-native';
import * as React from 'react';
import profile from './assets/profile.png';
import search from './assets/search.png';
import arrowRight from './assets/arrowRight.png';
import searchHighlight from './assets/searchHighlight.png';

import Menu from './Menu';

export default function Search({navigation}) {
  const [dontDisturb, setDontDisturb] = React.useState(false);
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <ScrollView
          style={styles.test}
          contentContainerStyle={{paddingBottom: 300}}>
          <View style={styles.header}>
            <View style={styles.logo}>
              <Text>STULEAD</Text>
            </View>
            <View style={styles.headerButton}>
              <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
                <Image source={profile} style={styles.profile} />
              </TouchableOpacity>
              <TouchableOpacity>
                <Image source={searchHighlight} style={styles.search} />
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.menu}>
            <TextInput
              placeholderTextColor="#333"
              style={styles.formInput}
              placeholder="Recherche"
            />
             <View style={styles.menuList}>
            <TouchableOpacity style={styles.menuItem}>
              <Text>Acheteur</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.menuItem}>
              <Text>Accompagnateur</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.menuItem}>
              <Text>Vendeur</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.menuItemBottom}>
              <Text>Créateur</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.menuList}>
            <TouchableOpacity style={styles.menuItem}>
              <Text>Création</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.menuItem}>
              <Text>Design</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.menuItem}>
              <Text>Graphisme</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.menuItemBottom}>
              <Text>Architect</Text>
            </TouchableOpacity>
          </View>
          </View>

        </ScrollView>
        <Menu />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  colors: {},
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignITems: 'center',
    width: '100%',
    height: 60,
    paddingHorizontal: 20,
  },
  headerButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '15%',
  },
  background: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: '100%',
    height: 300,
    marginTop: -20,
  },
  box: {
    width: '100%',
    height: '70%',
    justifyContent: 'space-between',
    padding: 20,
  },
  test: {
    width: '100%',
    height: 1000,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  formInput: {
    marginTop: 15,
    backgroundColor: 'white',
    borderRadius: 6,
    borderWidth: 1,
    borderColor: '#EE7CBD',
    height: 30,
    width: '90%',
    marginBottom: 15,
    paddingHorizontal: 10,
  },
  menu: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  menuList: {
    width: '90%',
    borderWidth: 3,
    borderColor: '#31B3BF',
    marginBottom: 20,
    marginTop:10,
    justifyContent:'center',
    alignItems:'center',
  },
  menuItem:{
    borderBottomWidth: 1,
    borderBottomColor: '#31B3BF',
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    width: '90%',
  },
  menuItemBottom:{

    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    width: '90%',
  }
});
