import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  Image,
} from 'react-native';
import * as React from 'react';
import intro from './assets/loginbg.png';

export default function ChooseLogin({navigation}) {
  return (
    <ImageBackground source={intro} style={styles.image}>
      <View style={styles.container}>
      <Text style={styles.text}>Le cupidon de l'orientation ! {'\n'}
 Venez découvrir de nouveau métier et vous aidez a trouver votre chemin.</Text>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('Log')}>
            <Text style={styles.buttonText}>Se connnecter</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('Login')}>
            <Text style={styles.buttonText}>S'inscrire</Text>
          </TouchableOpacity>

        </View>
      </View>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  colors: {
    blue: '#0F1C43',
    yellow: '#FBB709',
  },
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 100,
    left: 0,
    right: 0,
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
    marginTop: 20,
  },
button:{
  backgroundColor: '#1E1143',
  width: 120,
  height: 50,
  borderRadius: 150,
  alignItems: 'center',
  justifyContent: 'center',
},
buttonText:{
  color: '#fff',
  fontWeight: 'bold',
}, text: {
  color: '#1E1143',
  fontSize: 16,
  fontWeight: 'bold',
  textAlign: 'center',
  paddingHorizontal: 20,
}, 
});
