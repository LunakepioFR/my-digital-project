import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Image
  } from 'react-native';
  import * as React from 'react';
    import arrowBack from './assets/arrow-back.png';

  export default function Info2({navigation}) {
    const [situation, setSituation] = React.useState('');
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.back} onPress={() => navigation.navigate('Info1')}>
        <Image source={arrowBack} style={styles.arrowBack} />
        </TouchableOpacity>
        <View style={styles.header}>
            <View style={styles.dot} />
            <View style={styles.dotFull} />
            <View style={styles.dot} />
        </View>
        <View style={styles.box}>
            <Text style={styles.title}>Quelle est ta situation actuelle ?</Text>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity style={situation === '1' ? styles.boxYellow : styles.boxWhite} onPress={() => setSituation('1')}>
                        <Text style={situation === '1' ? styles.boxItemTitleYellow : styles.boxItemTitle}>Collégien-enne</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={situation === '2' ? styles.boxYellow : styles.boxWhite} onPress={() => setSituation('2')}>
                        <Text style={situation === '2' ? styles.boxItemTitleYellow : styles.boxItemTitle}>Lycéen-enne (voie général/technologique)</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={situation === '3' ? styles.boxYellow : styles.boxWhite} onPress={() => setSituation('3')}>
                        <Text style={situation === '3' ? styles.boxItemTitleYellow : styles.boxItemTitle}>Lycéen-enne (voie pro)</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={situation === '4' ? styles.boxYellow : styles.boxWhite} onPress={() => setSituation('4')}>
                        <Text style={situation === '4' ? styles.boxItemTitleYellow : styles.boxItemTitle}>Etudiant-e</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={situation === '5' ? styles.boxYellow : styles.boxWhite} onPress={() => setSituation('5')}>
                        <Text style={situation === '5' ? styles.boxItemTitleYellow : styles.boxItemTitle}>En activité</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={situation === '6' ? styles.boxYellow : styles.boxWhite} onPress={() => setSituation('6')}>
                        <Text style={situation === '6' ? styles.boxItemTitleYellow : styles.boxItemTitle}>En recherche d'emploi</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={situation === '7' ? styles.boxYellow : styles.boxWhite} onPress={() => setSituation('7')}>
                        <Text style={situation === '7' ? styles.boxItemTitleYellow : styles.boxItemTitle}>En recherche d'un stage/alternance</Text>
                    </TouchableOpacity>
            </View>
            <TouchableOpacity style={styles.buttonTwo} onPress={() => navigation.navigate('Info3') }>
                <Text style={styles.textButton}>Continuer</Text>
             </TouchableOpacity>
        </View>
        <View style={styles.precision}><Text style={styles.lowText}>Vous pouvez à tout moment changer vos{'\n'} informations dans les paramètres</Text></View>
      </View>
    );
  }

  const styles = StyleSheet.create({
    colors: {
      blue: '#0F1C43',
      yellow: '#FBB709',
    },
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    box: {
        height:'70%',
        width:'90%',
        borderWidth:2,
        borderColor:'#31B3BF',
        alignItems:'center',
        padding:20,
    },
    title:{
        fontSize:25,
        color:'#1E1143',
        fontWeight:'800',
        letterSpacing:-0.3,
        textAlign:'center',
    },
    header: {
        width:'100%',
        height:'15%',
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'row',
        position:'absolute',
        top:30,
    },
    dot: {
        width:15,
        height:15,
        borderRadius:50,
        borderWidth:1,
        borderColor:'#31B3BF',
        margin:5,
    },
    dotFull: {
        width:15,
        height:15,
        borderRadius:50,
        backgroundColor:'#EE7CBD',
        borderWidth:1,
        borderColor:'#31B3BF',
        margin:5,
    },
    formTitle:{
        fontSize:15,
        color:'#1E1143',
    },
    buttonTwo:{
        marginTop:35,
        backgroundColor: '#1E1143',
        justifyContent: 'center',
        alignItems: 'center',
        height: 30,
        width: 225,
        borderRadius: 6,
    },

    textButton: {
        color: '#fff',
        fontSize: 14,
        fontWeight: '700',
      },
      lowText: {
        fontSize: 9,
        color: '#1E1143',
        textAlign: 'center',
      },
      precision: {
        textAlign: 'center',
        position: 'absolute',
        bottom: 30,
      },
    buttonContainer: {
        marginTop:20,
        height:'70%',
        width:'100%',
    },
    boxWhite: {
        backgroundColor: '#fff',
        borderWidth:2,
        borderColor:'#EE7CBD',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 6,
        height: 35,
        marginTop:10,
        marginBottom:10,
    },
    boxYellow: {
        borderWidth:2,
        borderColor:'#EE7CBD',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 6,
        height: 35,
        marginTop:10,
        marginBottom:10,
        backgroundColor: '#EE7CBD',
    },
    boxItemTitleYellow:{
        color:'#0F1C43',
        fontWeight:'700',
    },
    boxItemTitle:{
        fontWeight:'700',
    },
    back:{
        position: 'absolute',
        top: 80,
        left: 40,
        zIndex:2,
    },
  });

